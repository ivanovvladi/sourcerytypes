//
//  AutoEmptyCallable.swift
//  SourceryTypes
//
//  Created by Vladislav Ivanov.
//  Copyright © 2021 Vladislav Ivanov. All rights reserved.
//

import Foundation

public protocol AutoEmptyCallable {}
